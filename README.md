# Point of Sale

## Overview
````
  A Point of Sale system is a fully integrated application that
  - allows any transaction,
  - automatically registers product moves in stock, and
  - gives real-time statistics and consolidations across   all shops.
  ````
### Configuration
* Make products available in PoS
````
  To make products available for sale -
    Point of Sale ‣ Products ‣ Products ‣ Open a product
  In Product details -
    Sales tab ‣ Enable Available in Point of Sale
  ````
* Configure payment methods
````
  To make payment methods -
    Point of Sale ‣ Configuration ‣ Payment Methods ‣ Create method
  To select the payment method -
    Point of Sale ‣ Configuration ‣ Point of Sale ‣ Open the PoS System ‣ Payment methods ‣ Add the payment method
  ````
### PoS Session
* Order products
````
  Point of Sale ‣ Dashboard ‣ New session ‣ PoS interface ‣ Order ‣ Payment ‣ Select payment method ‣ Enter the received amount ‣  Validate the payment
  ````
* Return and refund products
````
  PoS interface ‣ Select the product and quantity ‣ Modify with +/- button
  - For multiple products, repeat the process individually.
  - When on the payment interface, the total is negative. To end the refund, process the payment and validate it
  ````
* Close the PoS session
````
  PoS interface ‣ Close(upper right corner) ‣ Confirm
  Dashboard ‣ Close
  - Once a session is closed, a summary of all transactions per payment method will appear.
  Click on a line(to see all orders that were paid during the PoS session) ‣ Validate the session(if everything is correct) ‣ Post the closing entries
  ````
* View the statistics
````
  Point of Sales ‣ Reporting ‣ Orders
  Can also be accessed through the dashboard.
  ````
### Register customers
````
  Registering customers give the ability to grant them various privileges such as
  - discounts
  - loyalty program
  - specific communication
  - provide invoice
  - faster future interaction
  ````
* Create a customer
````
  Session interface ‣ Customer ‣ Create ‣ Save
  ````
### Secure connection (HTTPS)
````
  If Direct Devices is enabled in a Point of Sale settings(ePos printer), HTTP becomes the default protocol.
  ````
* Force Point of Sale to use a secure connection (HTTPS)
````
  Activate the developer mode ‣ Settings ‣ Technical ‣ Parameters ‣ System Parameters ‣ Create ‣ Save
  Parameters:
  - Key: point_of_sale.enforce_https
  - Value: True
  ````
### Self-signed certificate for ePOS printers
````
  - ePos printers are designed specifically to work with Point of Sale system, which sends the tickets directly to the printer.
  - Some models don’t require an IoT box, but may require HTTPS protocol.
  -  If so, a self-signed certificate is necessary to use printer.
  ````
* Generate a Self-signed certificate
````
  Access ePOS printer’s settings(with web browser by navigating IP) ‣ Authentication ‣ Certificate List ‣ Create ‣ Reboot  the printer ‣ Security ‣ SSL/TLS ‣ Select Selfsigned Certificate
  ````
* Export the Self-signed certificate
````
  To avoid several times acceptance -
  Access ePOS printer’s settings(navigating IP) ‣ Accept the self-signed certificate ‣ Connection is not secure ‣ Certificate is not valid ‣ Details tab ‣ Copy to file ‣ Select X.509 in base 64 ‣ Save
  ````
* Import the Self-signed certificate to Windows (Using Chrome)
````
  Chrome browser ‣ Settings ‣ Privacy and security ‣ Security ‣ Manage certificates ‣ Trusted Root Certification Authorities tab ‣ Import ‣ Select previous file ‣ Accept all warnings ‣ Restart browser
  ````
* Import the Self-signed certificate to your Android device
````
  Android device ‣ Settings ‣ Search for certificate ‣ Certificate AC ‣ Select the certificate
  ````

## Shop Features
### Invoice from the PoS interface
* Activate invoicing
````
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Bills & Receipts ‣ Check invoicing
  - choose in which journal the invoices should be created.
  ````
* Select a customer
````
  PoS session interface ‣ Customer ‣ Select or Create
  ````
* Invoice a customer
````
  Payment screen ‣ Select Invoice ‣ validate
  ````
* Retrieve invoices
````
  Out from interface -
    PoS interface ‣ Close ‣ Confirm
  Find all orders -
    Point of Sale ‣ Orders ‣ Orders ‣ Status tab ‣ Select Order(invoiced) ‣ Access invoice
  ````
### Set-up Cash Control in Point of Sale
````
  - to check the amount of the cashbox at the opening and closing
  - to ensure no error has been made and no cash is missing
  ````
* Activate Cash Control
````
  Point of Sales ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ Payments ‣ Check cash control ‣ Opening/Closing Values ‣ Create
  ````
* Start a session
````
  Set opening Balance(top right corner) ‣ Use default value/modify
  ````
* Close a session
````
  - In closing the session, the theoretical balance, the real closing balance and the difference between the two will appear.
  - Set Closing Balance/Take Money Out/Put Money In
  - If there's zero-sum difference and the same closing and opening balance, cashbox is ready for the next session.
  ````
### Using barcodes in PoS
````
  Using a barcode scanner
  - improves efficiency and
  - save time
  ````
* Configuration
````
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ IoT Box / Hardware ‣ Select Barcode Scanner
  ````
* Add barcodes to product
````
  Point of Sale ‣ Catalog ‣ Products ‣ Select product ‣ General information tab ‣ Set barcode
  ````
* Scanning products
````
  PoS interface ‣ Scan barcode(the product will be added)
  - scan the same product to add it multiple times or change the quantity manually on the screen.
  ````
### Log in with employee
````
  Features
  - manage multiple cashiers
  - keep track of who is working, when and how much  each cashier made for that session.
````
* Configuration
````
  PoS settings ‣ Check log in with employee ‣ Add the employees(that have access to the cash register)
  ````
* Switch without pin codes
````
  PoS Session interface ‣ Cashier name ‣ Select cashier
  ````
* Switch cashier with pin codes
````
  HR settings tab ‣ Employee form ‣ Add a security PIN
  - Now, when switching cashier, a PIN password will be asked.
  ````
* Switch cashier with barcodes
````
  Add the barcode(at the PIN code place) ‣ Print the badge ‣ Scan barcode(the cashier will be switched)
  ````
* Find who was the cashier
````
  Close PoS session ‣ Orders menu ‣ Open the order(summary of sold products)
  ````
### Reprint Receipts
* Configuration
````
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Bills & Receipts ‣ Check Reprint Receipt
  ````
* Reprint a receipt
````
  PoS session interface ‣ Reprint Receipt(reprint last receipt)
  ````
### Cash Rounding
````
  Cash rounding is required when
  - the lowest physical denomination of currency, or the smallest coin, is higher than the minimum unit of account
  - to round up or down the total amount of an invoice to the nearest five cents, when the payment is made in cash
  ````
* Configuration
````
  Point of Sale ‣ Configuration ‣ Settings ‣ Enable Cash Rounding ‣ Save
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Enable Cash Rounding
  To define the Rounding Method -
    Open the drop-down list ‣ Create and Edit ‣ Save (both the Rounding Method and Point of Sale settings)
  ````

## Restaurant Features
````
  Unique features for restaurant
  - floor and table management
  - bill splittin
  - print orders from the kitchen
  - tip
````
### Configuration
````
  To activate the bar/restaurant features-
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Open the PoS System ‣ Check Is a BarRestaurant
  ````
* Add a floor
````
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Select Table Management ‣ Floors ‣ create
  ````
* Add tables
````
  PoS interface ‣ Floors ‣ Edit Mode(create, move, modify)
  ````
* Register table orders
````
  PoS interface ‣ Click table ‣ Payment interface ‣ Order
  ````
* Transfer customer
````
  Select current table ‣ Transfer ‣ Select transferring table
  ````
* Register an additional order
````
  - use the + button to simultaneously proceed to another one
  - can shift between your orders and process the payment when needed
  ````
### Bill-splitting option
* Configuration
````
  To activate the Bill Splitting feature-
    Point of Sales ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ Bills & Receipts ‣ Check Bill Splitting
  ````
* Split a bill
````
  PoS interface ‣ Split ‣ Select from order items ‣ Process the payment
  ````
### Print the Bill
* Configuration
````
  To activate Bill Printing-
    Point of Sale ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ Bills & Receipts ‣ Check Bill Printing
  ````
* Split a Bill
````
  PoS interface ‣ Bill ‣ Print the bill
  ````
### Print orders at the kitchen or bar
* Activate the bar/kitchen printer
````
  To activate the Order printing feature-
    Point of Sales ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ IoT Box / Hardware Proxy ‣ Order Printer(need IoT Box to connect printer)
  ````
* Add a printer
````
  Point of Sales ‣ Configuration ‣ Order Printers ‣ Create
  ````
* Print the order in the kitchen/bar
````
  PoS interface ‣ Order(print the order on kitchen/bar printer)
  ````
### Using fiscal positions in PoS
````
  - apply different taxes based on the customer location
  - apply different taxes depending if the customer eats in or takes away
  ````
* Set up fiscal positions for PoS
````
  To enable this feature-
    Point of Sale ‣ Configuration ‣ Point of Sale ‣ Check Fiscal Position per Order ‣ Choose the fiscal positions
  ````
* Using fiscal positions
````
  PoS interface ‣ Tax ‣ Choose the fiscal position for the current order
  ````
* Set up a default fiscal position
````
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Enable Fiscal Position ‣ Choose one to set as the default one
  ````
### Integrate a tip option into payment
* Configuration
````
  To activate the Tips feature-
    Point of Sale ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ Bills & Receipts ‣ Check Tips ‣ Create a Tip Product
  ````
* Add Tips to the bill
````
  Payment interface ‣ Tip(add the tip and process the payment)
  ````
## Payment Terminals
````
  Payment terminals
  - offer fluid payment flow to customers
  - ease the work of cashiers
  ````
### Connect an Ingenico Payment Terminal to your PoS
* Configuration
````
  - Connect an IoT Box
  - Configure the Lane/5000 for Ingenico BENELUX
      Click F button of the terminal ‣ PoS Menu ‣ Settings ‣ Enter the settings password
      Click connection change and TCP/IP ‣ Type the IP of IoT Box ‣ Enter 9000 as port(terminal will restart)
      IoT Box form ‣ Verify terminal
  - Configure the payment method
      POS general settings ‣ Activate the Ingenico setting
      Point of Sale ‣ Configuration ‣ Point of Sale ‣ Payment Method ‣ Create new method ‣ Select payment terminal option "Ingenico" ‣ Select payment terminal device
  ````
* Pay with a payment terminal
````
  PoS Session Interface ‣ Process Payment ‣ Select Payment Method(using payment terminal) ‣ Send(checking that the amount in the tendered column is the one that has to be sent) ‣ Payment Successful Status(when the payment is successful)
  - To cancel the payment request, click on cancel, still can retry to send the payment request.
  - Force the payment using the Force Done, if there is any issue with the payment terminal(Odoo allow to validate the order).
  - Once payment is processed, the type of card that has been used and the transaction ID will be found on the payment record.
  ````
### Connect a Vantiv Payment Terminal to PoS
* Configuration
````
  Configure the payment method -
    POS general settings ‣ Activate the Vantiv setting
    Point of Sale ‣ Configuration ‣ Point of Sale ‣ Payment Method ‣ Create new method ‣ Select payment terminal option "Vantiv" ‣ Create new Vantiv credentials(merchant ID and password) ‣ Save ‣ Select credentials ‣ Save the payment methods
  ````
* Pay with a payment terminal
````
  PoS Session Interface ‣ Process Payment ‣ Select Vantiv Payment Method ‣ Proceed
  ````
### Connect a SIX Payment Terminal to PoS
* Configuration
````
  Configure the payment method -
    Apps ‣ Search POS Six module ‣ Install
    Point of Sale ‣ Configuration ‣ Point of Sale ‣ Payment Method ‣ Create new method ‣ Select payment terminal option "SIX without IoT Box" ‣ Enter payment terminal IP address ‣ Save
  ````
* Pay with a payment terminal
````
  PoS Session Interface ‣ Process Payment ‣ Select Payment Method ‣ Proceed(can reverse the last transaction)
  ````
## Pricing features
````
  Features
    - time-limited discount
    - seasonal discount
    - manual discount
  ````
### Apply manual discounts
````
  Apply a discount
    - on the whole order
    - specific products inside an order
  ````
* Apply a discount on a product
````
  PoS session interface ‣ Disc(input a discount over the currently selected product)
  ````
* Apply a global discount
````
  To apply a discount on the whole order-
    Point of Sales ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Pricing ‣ Check Global Discounts
    PoS interface ‣ Discount(enter the wanted discount)
  ````
### Apply time-limited discounts
* Configuration
````
  To activate time-limited discounts-
    Point of Sales ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Enable the pricelist feature ‣ Choose pricelists ‣ define a default one
  ````
* Create a pricelist
````
  Point of Sale ‣ Products ‣ Pricelists ‣ Create(specific price: period, min. quantity etc)
  ````
* Using a pricelist with the PoS interface
````
  PoS interface ‣ Pricelist button ‣ Select a pricelist(instantly update the prices with the selected pricelist then finalize the order)
  ````
### Using discount tags with a barcode scanner
````
  Product getting close to its expiration date, sell them with a discount using discount tags by scanning discount barcodes.
  ````
* Barcode Nomenclature
````
  Point of Sales ‣ Configuration ‣ Point of Sale ‣ Select PoS interface ‣ Barcode scanner ‣ Default Nomenclature
  For 50% discount on a product
    - start the barcode with 22 (for the discount barcode nomenclature) and then 50 (for the %) before adding the product barcode
  ````
* Scan the products & tags
````
  - scan the desired product
  - scan the discount tag(discount will be applied)
  ````
### Manage a loyalty program
* Configuration
````
  To activate the Loyalty Program feature-
    Point of Sale ‣ Configuration ‣ Point of sale ‣ Select PoS interface ‣ Pricing features ‣ Check Loyalty Program(create/ edit loyalty programs)
    The type of program
    - make reward specific(discount/gift) to some products or cover the whole range.
    - apply rules so that it is only valid in specific situation and everything in between.
  ````
* Use the loyalty program in your PoS interface
````
  PoS interface ‣ Rewards(enough points according to the rules in loyalty program)
  ````
* Using Pricelists in Point of Sale
````
  - happy hour(50% off/ buy one get one free)
  - multiple prices for the same product
  ````
* Set up Pricelists
````
  Point of Sale ‣ Configuration ‣ Configuration ‣ Enable Pricelist
  Point of Sale ‣ Configuration ‣ Point of Sale ‣ Enable Pricelist for the PoS ‣ Create(choose the product category for happy hour and the discount)
  PoS settings ‣ Pricelist ‣ Add the created pricelist
  PoS interface ‣ Select from pricelists
  ````
